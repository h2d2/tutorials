# Tutorials for H2D2

A step by step introduction to the possibilities of H2D2

1. Basic Advection-Diffusion 2D simulation
2. Non-Stationary Advection-Diffusion simulation
3. 2D Drying-wetting Shallow water simulation
4. Coupling 2D St. Venant with 2D Advection-Diffusion

See http://www.gre-ehn.ete.inrs.ca/H2D2/tutorial for the description